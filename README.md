# USDZ Converter

Convert 3D models to USDZ file format

## Usage

- Install Xcode cli tools via "xcode-select --install"

- Open "Build/USDZ Converter.app"

- Convert!
