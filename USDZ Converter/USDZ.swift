//
//  USDZ.swift
//  USDZ Converter
//
//  Created by Dominic Opitz on 14.06.19.
//  Copyright © 2019 doop. All rights reserved.
//

import Foundation

class USDZ {
    enum Key: String {
        case source = "usdz_converter",
        colorMap = "color_map",
        normalMap = "normal_map",
        emissiveMap = "emissive_map",
        metallicMap = "metallic_map",
        roughnessMap = "roughness_map",
        aoMap = "ao_map",
        opacity = "opacity"
    }
    
    struct Command {
        let key: Key
        var value: String
        var input: String {
            return "\(key.rawValue) \(value)"
        }
        
        init(key: Key, value: String) {
            self.key = key
            self.value = value
        }
    }
    
    static let targetType: String = "usdz"
    static let sourceTypes: [String] = ["obj", "abc", "gltf"]
    static let mapTypes: [String] = ["jpg", "png"]
    
    let source: Command
    let commands: [Command]
    
    init(source: Command, commands: [Command] = []) {
        self.source = source
        self.commands = commands
    }
    
    func convert() -> String {
//        xcrun usdz_converter ~/Desktop/quickLook/PokemonStatic.obj ~/Desktop/quickLook/usdz/PokemonStatic.usdz -color_map ~/Desktop/quickLook/textures/color.jpg -normal_map ~/Desktop/quickLook/textures/normal.jpg -roughness_map ~/Desktop/quickLook/textures/roughness.jpg -ao_map ~/Desktop/quickLook/textures/ao.jpg
        
//        xcrun usdz_converter ~/Desktop/quickLook/PokemonAnimated.abc ~/Desktop/quickLook/usdz/PokemonAnimated.usdz -x lvysaur -color_map ~/Desktop/quickLook/textures/color.jpg -normal_map ~/Desktop/quickLook/textures/normal.jpg -roughness_map ~/Desktop/quickLook/textures/roughness.jpg -ao_map ~/Desktop/quickLook/textures/ao.jpg -g snow -emissive_map ~/Desktop/quickLook/textures/white.jpg -roughness_map ~/Desktop/quickLook/textures/roughness_white.jpg`
        
        var params = String()
        for command in commands {
            params.append(" -\(command.input)")
        }
        let target = source.value.replacingOccurrences(
            of: ".\((source.input as NSString).pathExtension)",
            with: ".\(USDZ.targetType)")
        
        let input = "xcrun \(source.input) \(target)\(params)"
        let output = input.runAsCommand()
        print(output)
        
        return target
    }
}
