//
//  ViewController.swift
//  USDZ Converter
//
//  Created by Dominic Opitz on 13.06.19.
//  Copyright © 2019 doop. All rights reserved.
//

import Foundation
import AppKit

class ViewController: NSViewController {
    
    @IBOutlet weak var sourcePathControl: NSPathControl!
    @IBOutlet weak var sourceCheckbox: NSButton!
    @IBOutlet weak var colorMapPathControl: NSPathControl!
    @IBOutlet weak var colorMapCheckbox: NSButton!
    @IBOutlet weak var normalMapPathControl: NSPathControl!
    @IBOutlet weak var normalMapCheckbox: NSButton!
    @IBOutlet weak var emissiveMapPathControl: NSPathControl!
    @IBOutlet weak var emissiveMapCheckbox: NSButton!
    @IBOutlet weak var metallicMapPathControl: NSPathControl!
    @IBOutlet weak var metallicMapCheckbox: NSButton!
    @IBOutlet weak var roughnessMapPathControl: NSPathControl!
    @IBOutlet weak var roughnessMapCheckbox: NSButton!
    @IBOutlet weak var aoMapPathControl: NSPathControl!
    @IBOutlet weak var aoMapCheckbox: NSButton!
    @IBOutlet weak var convertButton: NSButton!
    @IBOutlet weak var opacityTextField: NSTextField!
    @IBOutlet weak var opacitySlider: NSSlider!

    private var urls: [USDZ.Key: (checkbox: NSButton, pathControl: NSPathControl)] = [:]
    private var basePathItems: [NSPathControlItem] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        basePathItems = sourcePathControl.pathItems
        urls[.source] = (checkbox: sourceCheckbox, pathControl: sourcePathControl)
        urls[.colorMap] = (checkbox: colorMapCheckbox, pathControl: colorMapPathControl)
        urls[.normalMap] = (checkbox: normalMapCheckbox, pathControl: normalMapPathControl)
        urls[.emissiveMap] = (checkbox: emissiveMapCheckbox, pathControl: emissiveMapPathControl)
        urls[.metallicMap] = (checkbox: metallicMapCheckbox, pathControl: metallicMapPathControl)
        urls[.roughnessMap] = (checkbox: roughnessMapCheckbox, pathControl: roughnessMapPathControl)
        urls[.aoMap] = (checkbox: aoMapCheckbox, pathControl: aoMapPathControl)
        
        for url in urls {
            url.value.pathControl.delegate = self
            url.value.pathControl.font = opacityTextField.font
            switch url.key {
            case .source:
                url.value.pathControl.allowedTypes = USDZ.sourceTypes
            default:
                url.value.pathControl.allowedTypes = USDZ.mapTypes
            }
        }
    }
    
    @IBAction func sliderDidChange(_ sender: NSSlider) {
        opacityTextField.stringValue = "Opacity (\(Int(sender.floatValue * 100)))"
    }
    
    @IBAction func pathControlDidSelect(_ sender: NSPathControl) {
        print("select: \(String(describing: sender.url))")
    }
    
    @IBAction func checkboxTapped(_ sender: NSButton) {
        guard sender.state == .off else {
            sender.state = .off
            return
        }
        
        let selection = urls.first(where: {$0.value.checkbox == sender})?.value
        selection?.pathControl.url = nil
        selection?.pathControl.pathItems = basePathItems
    }
    
    @IBAction func convertButtonTapped(_ sender: Any) {
        guard let source = sourcePathControl.url, !source.pathExtension.isEmpty else {
            return
        }
        
        let sourceCommand = USDZ.Command.init(key: .source, value: source.relativePath)
        var commands: [USDZ.Command] = []
        for item in urls {
            guard item.key != .source, let url = item.value.pathControl.url, !url.pathExtension.isEmpty else {
                continue
            }
            
            commands.append(USDZ.Command(key: item.key, value: url.relativePath))
        }
        commands.append(USDZ.Command(key: .opacity, value: "\(opacitySlider.floatValue)"))
        
        let usdz = USDZ(source: sourceCommand, commands: commands)
        let usdzPath = usdz.convert()
        let usdzURL = URL(fileURLWithPath: usdzPath)
        
        NSWorkspace.shared.open(usdzURL)
    }
}

extension ViewController: NSPathControlDelegate {
    func pathControl(_ pathControl: NSPathControl, acceptDrop info: NSDraggingInfo) -> Bool {
        guard let pasteboard = info.draggingPasteboard.propertyList(forType: NSPasteboard.PasteboardType(rawValue: "NSFilenamesPboardType")) as? NSArray?, let path = pasteboard?[0] as? String else {
                return false
        }
        
        let url = URL(fileURLWithPath: path)
        
        if (pathControl.allowedTypes ?? []).contains(url.pathExtension.lowercased()) {
            pathControl.url = url
            let selection = urls.first(where: {$0.value.pathControl == pathControl})
            selection?.value.checkbox.state = .on
            
            return true
        }
        
        return false
    }
    
    func pathControl(_ pathControl: NSPathControl, validateDrop info: NSDraggingInfo) -> NSDragOperation {
        return .every
    }
}
